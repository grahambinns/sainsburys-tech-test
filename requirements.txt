testtools==2.0.0
BeautifulSoup==3.2.1
requests==2.9.1
nose==1.3.7
mock==1.3.0
