# Sainsbury's Web Scraper - Technical Test

This is my solution to the problem posed in the developer technical test --
namely to produce a simple web scraper that, given a mock Sainsbury's page
URL, returns JSON about the products found on that page.


## Installation

I advise you to use a virtualenv to keep your system Python packages
untouched, but this will work just as well without one (you will need sudo
rights to install the dependencies).

To install this Python app's dependencies, run the the command

    pip install -r requirements.txt

In the root directory of the repository. This will install the following
dependencies, as described in requirements.txt:

 - BeautifulSoup - used for HTML parsing
 - mock - used for tests
 - nose - used for tests
 - requests - used for fetching HTML
 - testtools - used for tests


## Running the tests

Once you've installed all dependencies, you can run the application's unit
tests using nose:

    $ nosetests
    ..............
    ----------------------------------------------------------------------
    Ran 14 tests in 0.099s


## Running the application

The application can be found in `src/scrape.py`. You can run it using the
Python interpreter:

    $ python src/scrape.py
    {
        "results": [
            {
                "description": "Apricots",
                "size": "38.27KB",
                "title": "Sainsbury's Apricot Ripe & Ready x5",
                "unit_price": 3.5
            },
            ...
        ]
    }
