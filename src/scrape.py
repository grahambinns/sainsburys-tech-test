# -*- coding: utf-8 -*-
"""Sainsbury's web-scraper technical test.

:author: Graham Binns <graham.binns@gmail.com>
"""
from __future__ import print_function
import json
import re

from BeautifulSoup import BeautifulSoup
import requests


def get_product_data_from_page(product_data_soup):
    """Return the product data in `product_data_soup` as a dict.

    :param product_data_soup: A BeautifulSoup instance containing the
                              product data HTML.
    :return: A dict containing the product title, unit priceand description.
    """
    title = product_data_soup.find(
        "div", {"class": "productTitleDescriptionContainer"}).find("h1").text
    unit_price_string = product_data_soup.find(
        "p", {"class": "pricePerUnit"}).text

    # We can extract the unit price from the unit_price_string using a
    # regexp.
    # Note that when this is output to JSON, trailing zeroes will be
    # lost.
    unit_price = float(re.search("\d+\.\d+", unit_price_string).group())

    # There may be multiple productText elements found on the page; we
    # only want the first one.
    description = product_data_soup.find("div", {"class": "productText"}).text

    return {
        "title": title,
        "unit_price": unit_price,
        "description": description,
    }


def get_product_data(url):
    """Given the URL of a product page, return a data dict about that product.

    :param url: The URL for the product page.
    :return: A dict containing the product title, unit price and
             description, and the size in KB* of the product page.
             * NB: The spec for this project says the size in 'kb'. I've
                   chosen to assume that this means kilobytes (KB)
                   though it could equally mean kilobits (kb). Since
                   it's ambiguous I chose to use the unit that made more
                   sense in context.
    """
    page_data = requests.get(url).content
    product_data = get_product_data_from_page(BeautifulSoup(page_data))

    # To get the size of the page in KB we divide by 1024.0 -- this
    # ensure that we capture part-KBs correctly.
    product_data["size"] = "{0:.2f}KB".format(len(page_data) / 1024.0)

    return product_data


def get_product_list(url):
    """Get a list of products from `url`.

    :param url: The URL from which to load a list of products.
    :return: A list of dicts, each dict containing the keys described in
            `get_product_list().`
    """
    page_data = requests.get(url).content
    page_data_soup = BeautifulSoup(page_data)
    return_value = []
    product_info_list = page_data_soup.findAll("div", {"class": "productInfo"})
    for product_div in product_info_list:
        product_url = product_div.find("h3").find("a")["href"]
        product_data = get_product_data(product_url)
        return_value.append(product_data)
    return return_value


def main():
    """Main entrypoint for scrape.py."""
    url = (
        "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/"
        "2015_Developer_Scrape/5_products.html")
    product_list = get_product_list(url)
    print(
        json.dumps({
            "results": product_list
        }, sort_keys=True, indent=4,
        separators=(',', ': ')))


if __name__ == "__main__":
    main()
