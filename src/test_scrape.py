# -*- coding: utf-8 -*-
"""Tests for scrape.py -- Sainsbury's web-scraper technical test.

:author: Graham Binns <graham.binns@gmail.com>
"""
from random import (
    choice,
    randint,
)
import string
from textwrap import dedent

from BeautifulSoup import BeautifulSoup
from mock import (
    call,
    Mock,
)
import scrape
from testtools import TestCase
from testtools.matchers import (
    Equals,
    IsInstance,
    KeysEqual,
)

# Shim to ensure that all classes descend from object.
__metaclass__ = type


# An HTML blob that contains the data to be used for our unit tests.
# This is taken from the example HTML at
# http://hiring-tests.s3-website-eu-west-1.amazonaws.com/.  For the sake
# of brevity, this contains _only_ the necessary data for
# get_product_data_from_page() to operate. All extraneous data has been
# discarded.
SAMPLE_PRODUCT_HTML = dedent("""\
    <div class="productSummary">
        <div class="productTitleDescriptionContainer">
            <h1>Sainsbury's Apricot Ripe &amp; Ready x5</h1>
        </div>
        <div class="addToTrolleytabContainer addItemBorderTop">
            <div class="priceTab activeContainer priceTabContainer"
                    id="addItem_149117">
                <div class="pricing">
                    <p class="pricePerUnit">
                        £3.50<abbr title="per">/</abbr><abbr title="unit">
                        <span class="pricePerUnitUnit">unit</span></abbr>
                    </p>

                    <p class="pricePerMeasure">£0.70<abbr title="per">/</abbr>
                        <abbr title="each">
                        <span class="pricePerMeasureMeasure">ea</span></abbr>
                    </p>
                </div>
        </div>
    </div>
    <div class="productText">
        <p>Apricots</p>
    </div>
""")


def make_random_string():
    """Make a string of a random characters return it."""
    return "".join([choice(string.ascii_letters) for i in range(10)])


class GetProductDataFromPageTestCase(TestCase):
    """Tests for scrape.get_product_data_from_page()."""

    def _make_sample_soup(self, sample_html=None):
        """Return a BeautifulSoup instance containing `sample_html`.

        If `sample_html` is not specified, use the contents of
        SAMPLE_PRODUCT_HTML.

        :param sample_html: The sample HTML string to wrap in a
                            BeautifulSoup instance.
        :return: BeautifulSoup
        """
        if sample_html is None:
            sample_html = SAMPLE_PRODUCT_HTML
        # We _could_ use mock to mock out BeautifulSoup behaviour, but that
        # seems overkill -- and also rather complex given the nested nature
        # of BeautifulSoup queries.
        return BeautifulSoup(sample_html)

    def test_returns_dict_with_necessary_keys(self):
        # scrape.get_product_data_from_page() returns a dict with the keys
        # 'title', 'unit_price', 'size' and 'description'.
        return_value = scrape.get_product_data_from_page(
            self._make_sample_soup())
        self.expectThat(return_value, IsInstance(dict))
        self.expectThat(
            return_value,
            KeysEqual("title", "unit_price", "description"))

    def test_title_field_contains_product_title(self):
        # The `title` field of the dict returned by
        # get_product_data_from_page() contins the title of the product,
        # as found in the productTitleDescriptionContainer element in
        # the product HTML.
        product_dict = scrape.get_product_data_from_page(
            self._make_sample_soup())
        expected_title = "Sainsbury's Apricot Ripe &amp; Ready x5"
        self.assertEqual(expected_title, product_dict["title"])

    def test_unit_price_field_contains_product_unit_price(self):
        # The `unit_price` field of the dict returned by
        # get_product_data_from_page() contins the title of the product,
        # as found in the pricePerUnit element in the product HTML.
        product_dict = scrape.get_product_data_from_page(
            self._make_sample_soup())
        expected_price = 3.5
        self.expectThat(expected_price, IsInstance(float))
        self.expectThat(expected_price, Equals(product_dict["unit_price"]))

    def test_description_field_contains_product_description(self):
        # The `description` field of the dict returned by
        # get_product_data_from_page() contains the description of the
        # product.
        product_dict = scrape.get_product_data_from_page(
            self._make_sample_soup())
        expected_description = "Apricots"
        self.assertEqual(expected_description, product_dict["description"])

    def test_ignores_extra_product_text_elements_on_page(self):
        # There may be multiple productText elements on a page, but
        # get_product_data_from_page() is only interested in the first
        # one, which contains the product description.
        sample_html = SAMPLE_PRODUCT_HTML + '<div class="productText">Ignored</div>'
        product_dict = scrape.get_product_data_from_page(
            BeautifulSoup(sample_html))
        expected_description = "Apricots"
        self.assertEqual(expected_description, product_dict["description"])


class GetProductDataTestCase(TestCase):
    """Tests for scrape.get_product_data()."""

    def setUp(self):
        super(GetProductDataTestCase, self).setUp()
        self.patch(scrape.requests, "get", Mock())
        scrape.requests.get.return_value.content = SAMPLE_PRODUCT_HTML
        self.mock_get = scrape.requests.get

        self.patch(scrape, "BeautifulSoup", Mock())
        scrape.BeautifulSoup.return_value = BeautifulSoup(SAMPLE_PRODUCT_HTML)
        self.mock_soup = scrape.BeautifulSoup

    def test_fetches_html_from_passed_url(self):
        # get_product_data() fetches the the URL passed to it using
        # the requests module.
        url = "http://example.com"
        scrape.get_product_data(url)
        self.mock_get.assert_called_once_with(url)

    def test_calls_get_product_data_from_page(self):
        # get_product_data() passes the HTML it fetches from `utl` to
        # get_product_data_from_page() as a BeautifulSoup instance.

        # Patch BeautifulSoup so that we can properly check what's being
        # passed to get_product_data_from_page().
        self.patch(
            scrape, "get_product_data_from_page", Mock(return_value={}))
        scrape.get_product_data("http://example.com")

        scrape.get_product_data_from_page.assert_called_once_with(
            self.mock_soup.return_value)

    def test_returns_dict_with_necessary_keys(self):
        # get_product_data() returns a dict with the keys 'title',
        # 'unit_price', 'size' and 'description'.
        return_value = scrape.get_product_data("http://example.com")
        self.expectThat(return_value, IsInstance(dict))
        self.expectThat(
            return_value,
            KeysEqual("title", "unit_price", "description", "size"))

    def test_return_dict_contains_product_data_from_page(self):
        # get_product_data() returns in its return dict the details
        # extracted from the product data page by
        # get_product_data_from_page().
        mock_data = {
            "description": "spam",
            "title": "eggs",
            "unit_price": 42.00,
        }
        self.patch(
            scrape, "get_product_data_from_page",
            Mock(return_value=mock_data))

        return_value = scrape.get_product_data("http://example.com")
        self.expectThat(return_value["title"], Equals(mock_data["title"]))
        self.expectThat(
            return_value["unit_price"], Equals(mock_data["unit_price"]))
        self.expectThat(
            return_value["description"], Equals(mock_data["description"]))

    def test_return_dict_contains_size_of_fetched_html(self):
        # The `size` field of the dict returned by `get_product_data()`
        # contains the size in KB of the HTML fetched from the passed
        # URL.
        expected_size = "{0:.2f}KB".format(len(SAMPLE_PRODUCT_HTML) / 1024.0)
        return_value = scrape.get_product_data("http://example.com")
        self.assertEqual(expected_size, return_value["size"])


class GetProductListTestCase(TestCase):
    """Tests for scrape.get_product_list."""

    def setUp(self):
        super(GetProductListTestCase, self).setUp()
        # Create a random number of items in the list. This kind of
        # fuzzing ensures that we're not testing for arbirary specifics
        # that could be defeated by lazy coding.
        sample_product_list_item = dedent("""\
        <div class="productInfo">
            <h3><a href="{url}">{product_name}</a></h3>
        </div>
        """)
        sample_product_urls = [
            "http://{0}".format(make_random_string())
            for i in range(randint(1, 10))]
        sample_product_list = "".join([
            sample_product_list_item.format(
                url=url, product_name=make_random_string())
            for url in sample_product_urls])

        self.patch(scrape.requests, "get", Mock())
        scrape.requests.get.return_value.content = sample_product_list
        self.mock_requests_get = scrape.requests.get

        self.patch(scrape, "get_product_data", Mock())
        self.mock_get_product_data = scrape.get_product_data
        self.sample_product_urls = sample_product_urls

    def test_returns_list(self):
        self.assertIsInstance(
            scrape.get_product_list("http://example.com"), list)

    def test_fetches_html_from_passed_url(self):
        # get_product_list() fetches the the URL passed to it using the
        # requests module.
        url = "http://example.com"
        scrape.get_product_list(url)
        self.mock_requests_get.assert_called_once_with(url)

    def test_calls_get_product_data_with_all_product_data_urls(self):
        # for each product URL in the product list page,
        # get_product_list() calls get_product_data()
        scrape.get_product_list("http://example.com")
        expected_calls = [call(url) for url in self.sample_product_urls]
        self.mock_get_product_data.assert_has_calls(expected_calls)

    def test_returns_values_from_get_product_data(self):
        # The list returned by get_product_list() contains the results
        # of calls to get_product_data.
        expected_results = [
            {"title": make_random_string()}
            for i in range(len(self.sample_product_urls))]
        scrape.get_product_data.side_effect = expected_results
        product_list = scrape.get_product_list("http://example.com")
        self.expectThat(
            len(product_list), Equals(len(self.sample_product_urls)))
        self.expectThat(product_list, Equals(expected_results))
